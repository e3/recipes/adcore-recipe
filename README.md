# ADCore conda recipe

Home: "https://github.com/areaDetector/ADCore"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADCore module
