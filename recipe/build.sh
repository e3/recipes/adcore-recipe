#!/bin/bash

# LIBVERSION shall only include MAJOR.MINOR.PATCH for require
LIBVERSION=${PKG_VERSION}
E3_ADCORE_LOCATION="${EPICS_MODULES}/${PKG_NAME}/${PKG_VERSION}"

# Clean between variants builds
make clean

make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db_internal
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install

# Create activate/deactivate scripts
mkdir -p $PREFIX/etc/conda/activate.d
cat <<EOF > $PREFIX/etc/conda/activate.d/adcore_activate.sh
export E3_ADCORE_LOCATION="${E3_ADCORE_LOCATION}"
EOF

mkdir -p $PREFIX/etc/conda/deactivate.d
cat <<EOF > $PREFIX/etc/conda/deactivate.d/adcore_deactivate.sh
unset E3_ADCORE_LOCATION
EOF
